Real to Bin(R2B) inter-residue distance Converter

This project provides the programs and related files of R2B that convert protein real-values distances into bin probabilities.


Requirements
To run the program, for each protein named protein_name, you need the following files:
	protein_name.npy is the real-valued distance matrix predicted by MDP 
	protein_name.npz is an npz file predicted by trRosettaX


All of these files must be in a folder named by protein_name, and the folder is in the input directory.

To get the .npy, and .npz files, use the following servers
	.npy - https://gitlab.com/mahnewton/mdp
	.npz - https://yanglab.nankai.edu.cn/trRosetta/
	

Running the Program
	Update the protein_list.lst file with the names of the proteins needed to predict
	Put all necessary files for each protein in a separate folder named by the protein name inside the input folder
	Run bash predict.sh from the root folder 
	The outputs are generated in the output folder: protein_name_sc.npz or protein_name_fc.npz file for each protein
