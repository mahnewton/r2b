#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%% Env check 
import sys
if sys.version_info < (3,0,0):
    print('Python 3 required!!!')
    sys.exit(1)

#%% parse input-output options
import os
import getopt


def show_usage_info():
    print('Usage:' + sys.argv[0] + ' -d data_directory -o output_directory -c Conversion_Type -t Probability_Threshold')
    print('''
          Distance Type (-c): 
              sc = Skewed Conversion
              fc = Fixed Conversion
          ''')

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:o:c:t:h")
except getopt.GetoptError as err:
    print(err)
    show_usage_info()
    sys.exit(2)

for opt, opt_val in opts:
    if opt in ("-h", "--help"):
        show_usage_info()
        sys.exit()
    elif opt in ("-d"):
        data_dir = os.path.abspath(opt_val)    
    elif opt in ("-o"):
        output_dir = os.path.abspath(opt_val)    
    elif opt in ("-c"):
        conversion_type = 'sc' if opt_val == 'sc' else 'fc' # s: short distance prediction; l = long distance prediction;
    elif opt in ("-t"):
        threshold_prob = opt_val
        threshold_prob = float(opt_val) 
    else:
        print('Unhandled option provided.')
        show_usage_info()
        sys.exit(1)

def is_all_data_exists(data_dir):
    pid = data_dir.split('/')[-1]
    mdp_file = os.path.join(data_dir, pid) + '.npy'
    trX_file = os.path.join(data_dir, pid) + '.npz'

    
    if os.path.exists(mdp_file) and os.path.exists(trX_file):
        return True
    else:
        print("One or more data file missing. Check all files present in the target protein's directory.")
        print("Target protein's data directory: " + data_dir)
        return False


if not is_all_data_exists(data_dir):
    exit(1)
if not os.path.exists(output_dir):
    print('Output path not exists at: ' + output_dir)
    exit(1)

#%% imports
import numpy as np


#%% 
def SC_method(mdp_dist, bins_ranges, step, max_val, threshold_prob):
        L = len(mdp_dist[:, 0])
        bin_no = len(bins_ranges) + 1
        bins = np.arange(1, bin_no, 1)
        B = np.full((L, L, bin_no), 0.001)
        middle_prob = threshold_prob
        lowerupper_prob = round(1 - middle_prob, 2)
        binrange = step
        for i in range(L):
            for j in range(L):
                if mdp_dist[i, j] > max_val:
                    B[i, j, 0] = 0.99
                else:
                    for indx_bin, bin_i in enumerate(bins_ranges):
                        #print(indx_bin, bin_i)
                        if mdp_dist[i, j] > bin_i and mdp_dist[i, j] <= bin_i+step:
                            if bins[indx_bin] > 1 and bins[indx_bin] < 36:
                                lowerbintopreddist = mdp_dist[i, j] - bin_i                             
                                upperbintopreddist = (bin_i+binrange) - mdp_dist[i, j] 
                                lowebincoverage = 100 - ((lowerbintopreddist/binrange)*100)
                                upperbincoverage = 100 - ((upperbintopreddist/binrange)*100)
                                lower_binprob = (lowebincoverage/100)*lowerupper_prob
                                upper_binprob = (upperbincoverage/100)*lowerupper_prob
                                
                                B[i, j, bins[indx_bin]] = middle_prob
                                B[i, j, bins[indx_bin]-1] = lower_binprob
                                B[i, j, bins[indx_bin]+1] = upper_binprob

                            elif bins[indx_bin] == 1:
                                B[i, j, bins[indx_bin]] = middle_prob + lower_binprob
                                B[i, j, bins[indx_bin]+1] = upper_binprob

                            else:
                                B[i, j, bins[indx_bin]] = middle_prob + upper_binprob
                                B[i, j, bins[indx_bin]-1] = lower_binprob
                        
        return B
        
        
#%%
def FC_method(mdp_dist, bins_ranges, max_val, threshold_prob):
        L = len(mdp_dist[:, 0])
        bin_no = len(bins_ranges) + 1
        bins = np.arange(1, bin_no, 1)
        B = np.full((L, L, bin_no), 0.001)
        middle_prob = threshold_prob
        lowerupper_prob = round(1 - middle_prob, 2)
        for i in range(L):
            for j in range(L):
                if mdp_dist[i, j] > max_val:
                    B[i, j, 0] = 0.99
                else:
                    for indx_bin, bin_i in enumerate(bins_ranges):
                        #print(indx_bin, bin_i)
                        if mdp_dist[i, j] > bin_i and mdp_dist[i, j] <= bin_i+step:
                            if bins[indx_bin] > 1 and bins[indx_bin] < 36:                                
                                B[i, j, bins[indx_bin]] = middle_prob
                                B[i, j, bins[indx_bin]-1] = lowerupper_prob/2
                                B[i, j, bins[indx_bin]+1] = lowerupper_prob/2
                                #print(bins[indx_bin]+1)
                            elif bins[indx_bin] == 1:
                                B[i, j, bins[indx_bin]] = middle_prob + lowerupper_prob/2
                                B[i, j, bins[indx_bin]+1] = lowerupper_prob/2
                     
                            else:
                                B[i, j, bins[indx_bin]] = middle_prob + lowerupper_prob/2
                                B[i, j, bins[indx_bin]-1] = lowerupper_prob/2
                                
        return B



#%%
pid = data_dir.split('/')[-1]
mdp_file = os.path.join(data_dir, pid) + '.npy'
trX_file = os.path.join(data_dir, pid) + '.npz'
mdp_dist = np.load(mdp_file)
npz = np.load(trX_file)
dist,omega,theta,phi = npz['dist'],npz['omega'],npz['theta'],npz['phi']
fasta_file = os.path.join(data_dir, pid) + '.fasta'

#%% global vars
step = 0.5
max_val = 20
bins_ranges = np.arange(2, max_val, step)


#%%
if conversion_type == 'sc':
    binned_dist = SC_method(mdp_dist,bins_ranges, step, max_val,threshold_prob)
    np.savez(output_dir +'/'+ pid +'_sc.npz', dist = binned_dist, omega = omega, theta = theta, phi = phi)
    print('Skewed Conversion of '+pid+' completed!!!!!!!!!!!')
    
else:
    binned_dist = FC_method(mdp_dist, bins_ranges, max_val, threshold_prob)
    np.savez(output_dir +'/'+ pid +'_fc.npz', dist = binned_dist, omega = omega, theta = theta, phi = phi)
    print('Fixed Conversion of '+pid+' completed!!!!!!!!!!!')
    
