#!/bin/bash

while read id; do
    echo $id
    python ./scripts/R2B.py -d ./input/$id -o ./output  -c fc -t 0.7    # sc: Skewed Conversion; fc = Fixed Conversion;   -t is the probability threshold value
done < 'protein_list.lst'
